#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
@author: niels
Adds all GODM annotation (gold, silver bronze) to a table containing PFAM IDs
Input files are 3 files with PFAM-GO mapping from GODM and a table containing
a column with one or more PFAM IDs (works for both domains and architecture)
"""
import re
import pandas as pd

data_dir ='data/'


# INPUT & OUTPUT
# bp =biological process, cc=cellular compont, mc = molecular component
data_dir ='data/'
godm_input_files = ['pfam_gobp_most_specific.txt','pfam_gocc_most_specific.txt',
'pfam_gomf_most_specific.txt']


# ************** Change the lines below for your INPUT & Output **************
in_file = 'architectures_Streptococcus_All.csv'
out_file = 'architectures_Streptococcus_All2.csv'
pfam_column = 4 #Enter the number of the column containing GO IDs
go_column = 8 #Enter the number of the column containingPFAM IDs
seperator = '\t' #Enter the seperator type for your input file
seperator_go = ';'#Enter the seperator to put between multiple GO annotation
seperator_pfam =';'
skipLines = 1 #Skip header lines

#==========================================================================

# 1) Make pfam_go_dict based on GODM pfam_gofm
pfam_go_dict_godm ={}
for file_path in godm_input_files:
    with open(data_dir+file_path,'r') as tsv1:
        lines = [line.strip(' ') for line in tsv1]
        # input file does not contain proper seperators, use regular expr.
        for i in range(0,len(lines)):  #Skip first 7 header lines
            if i <1:
                continue #skip header
            line = lines[i]
            line_fields=line.split(';')
            go = line_fields[0]
            pfam = line_fields[1]
            # Add to PFAM-GO association dictionary if new    
            if pfam not in pfam_go_dict_godm.keys():
                pfam_go_dict_godm[pfam] = [go]
            else:
                pfam_go_dict_godm[pfam].append(go)
 

# 2) Combine old and new GO annotation from GODM and save output       
nr_new_go = 0 
nr_new_go_count_dict ={}
with open(in_file,'r') as tsv_in, open(out_file,'w') as tsv_out:
    lines = [line.split(seperator) for line in tsv_in]
    for i in range(0,len(lines)):
        line = lines[i]
        if i == 0:  # Skip firts header line
            tsv_out.write(seperator.join(line)) 
            continue
        # Retrieve all PFAM IDS in column via regular expression
        pfams = re.findall('PF+[0-9]+',line[pfam_column]) 
        gos = re.findall('GO:+[0-9]+',line[go_column])
        # Combine existing go annotation with GoDM GO annotation
        for pfam in pfams:
            if pfam in pfam_go_dict_godm.keys():
                gos_godm = pfam_go_dict_godm[pfam]
                new_go=[]
                for go in gos_godm:
                    if go not in gos:
                        new_go.append(go)
                        nr_new_go+=1
                line[go_column] = line[go_column]+seperator_go+seperator_go. \
                                    join(new_go)
                line[go_column] = line[go_column].rstrip(seperator_go)
        tsv_out.write(seperator.join(line))
    print('done adding {} GODM PFAM-GO annotations'.format(nr_new_go)) 
print(nr_new_go_count_dict)


    
