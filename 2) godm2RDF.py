#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 16 09:05:12 2018
Scripts creats rdf (turtle) triples linking godm annotation as external 
reference to pfam id's. 
The RDF ontology/structure is visualized in:GODM_RDF_structure.odt
GODM source files can be found and updated from http://godm.loria.fr/
Notes, this script might take a few minutes to run
@author: niels
"""

#The packages below can easily installed using pip install packageName
import pandas as pd
from rdflib import Namespace, Graph, URIRef, Literal
from rdflib.namespace import RDF, FOAF  #Load some common name spaces

def col2rdf(sub,pred,obj):
    '''
    Takes three dataframe column, makes them all in similar triples
    '''
    for i in range(0,len(sub)):
        #Creates subject predicate object
        s = URIRef(sub[i])
        p = URIRef(pred[i])
        o = URIRef(obj[i])
        #Add triple
        g.add( (s, p, o)  )

        
def literal2rdf(sub,pred,obj):
    '''
    Takes three dataframe column with the object column being stored as 
    Literal/Value
    '''
    for i in range(0,len(sub)):
        #Creates subject predicate object
        s = URIRef(sub[i])
        p = URIRef(pred[i])
        o = Literal(obj[i])
        #Add triple
        g.add( (s, p, o )) #Literal means its a value e.g. string, int, float   


# =============================================================================
# 1 ) Read in file
directory ='data/'
inFileList = ['pfam_gobp_most_specific.txt','pfam_gocc_most_specific.txt',
'pfam_gomf_most_specific.txt']
gbol ="http://gbol.life/0.1/"  # Use as prefix for most
g = Graph()  # Graph to store triples in 


# 3) Loop over the files with, load them in DF and make some tripples
for file1 in inFileList:
    filePath = directory+file1
    df = pd.read_csv(filePath, delimiter=';', encoding='utf-8')
    # a) Start making triples via custom collumns
    df['PFAM_URL']  = gbol+'db/pfam/'+ df['PFAM'].astype(str)
    df['GODM_URL']    = gbol+'db/godm/'+ df['PFAM'].astype(str)+"/"+ \
                        df['GO'].astype(str).replace(":","") #UniquePFAM-GO url
    df['xrev'] = gbol+'xrev'
    # Add preddicate for PFAM to GO  with gb/godm/ as database
    col2rdf(df['PFAM_URL'], df['xrev'],df['GODM_URL'])
    # Add GO gbol:Accession GO string
    df['accession'] = gbol+'accession'
    col2rdf(df['GODM_URL'], df['accession'], df['GO'])
    # Add triple to gbol:go gbol/db 'db/godm
    df['db'] = gbol+'db'
    df['db/godm']  = gbol+'db/godm'
    col2rdf(df['GODM_URL'], df['db'], df['db/godm'])
    # Add tripe  gbol:database gbol:id 'godm
    df['id'] = gbol+'id'
    df['godm'] = 'godm'
    literal2rdf(df['db/godm'],df['id'], df['godm'])
    # Add triple for gbol:db/go rdf:type gbol:Database
    df['type'] =  RDF.type
    df['Database']    = gbol+'Database'
    col2rdf(df['db/godm'], df['type'],df['Database'])
    # Add triple for class, bronze, silve, gold
    df['class'] = gbol+'class'
    col2rdf(df['GODM_URL'], df['class'], df['CLASS'])
    # Add consensus score ass provenance
    df['consensusScore']  = gbol+'consensusScore'        
    literal2rdf(df['GODM_URL'],df['consensusScore'],df['CONSENSUS_SCORE'])


outFile ='godm.ttl'
output = open(outFile, 'w')
output.write(g.serialize(format='turtle'))
print('done converting to RDF, output: {}'.format(outFile))

